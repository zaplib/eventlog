﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Eventlog
{
    public class MyLog
    {
        public void Event(Exception ex, EventLogEntryType type = EventLogEntryType.Error,
            [System.Runtime.CompilerServices.CallerMemberName] string ProjectName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string CodePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int LineNumber = 0)
        {
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                string AppName = AppDomain.CurrentDomain.FriendlyName;
                string CallingAssembly = Assembly.GetCallingAssembly().GetName().Name;
                string CallingAssemblyPath = Assembly.GetCallingAssembly().Location;
                string ExecAssembly = Assembly.GetExecutingAssembly().FullName;
                string ExecAssemblyPath = Assembly.GetExecutingAssembly().Location;
                List<string> data = new List<string>();
                data.Add("Process = " + AppName);
                data.Add("CallingAssembly = " + CallingAssembly);
                data.Add("CallingAssemblyPath = " + CallingAssemblyPath);
                data.Add("ExecAssembly = " + ExecAssembly);
                data.Add("ExecAssemblyPath = " + ExecAssemblyPath);
                data.Add("CodePath = " + CodePath);
                data.Add("Method = " + ProjectName);
                data.Add("Log At Line = " + LineNumber + "");
                data.Add("Message = " + ex.ToString());
                EventInstance eventInstance = new EventInstance(0, 0, type);
                eventLog.WriteEvent(eventInstance, data.ToArray());
            }

        }
        public void log(string Message, EventLogEntryType type = EventLogEntryType.Information,
            [System.Runtime.CompilerServices.CallerMemberName] string ProjectName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string CodePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int LineNumber = 0)
        {
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";

                string AppName = AppDomain.CurrentDomain.FriendlyName;
                string CallingAssembly = Assembly.GetCallingAssembly().GetName().Name;
                string CallingAssemblyPath = Assembly.GetCallingAssembly().Location;
                string ExecAssembly = Assembly.GetExecutingAssembly().FullName;
                string ExecAssemblyPath = Assembly.GetExecutingAssembly().Location;

                List<string> data = new List<string>();
                data.Add("Process = " + AppName);
                data.Add("CallingAssembly = " + CallingAssembly);
                data.Add("CallingAssemblyPath = " + CallingAssemblyPath);
                data.Add("ExecAssembly = " + ExecAssembly);
                data.Add("ExecAssemblyPath = " + ExecAssemblyPath);
                data.Add("CodePath = " + CodePath);
                data.Add("ProjectName = " + ProjectName);
                data.Add("Line = " + LineNumber + "");
                data.Add("Message = " + Message);
                //object[] data = new object[] { new XElement("Data", new XElement("Data", new XAttribute("name", "QAQ"))) };
                EventInstance eventInstance = new EventInstance(0, 0, type);
                eventLog.WriteEvent(eventInstance, data.ToArray());
            }
        }
    }
}

/*
    wmplayer.exe 
   12.0.17763.1 
   e39954c1 
   ntdll.dll 
   10.0.17763.292 
   f3450dbf 
   c0000005 
   00045dbd 
   dfc 
   01d4dd34bec93d0d 
   C:\Program Files (x86)\Windows Media Player\wmplayer.exe 
   C:\WINDOWS\SYSTEM32\ntdll.dll 
   74487bb2-a784-43e9-80f8-c301ce211e53  

失敗的應用程式名稱: wmplayer.exe，版本: 12.0.17763.1，時間戳記: 0xe39954c1
失敗的模組名稱: ntdll.dll，版本: 10.0.17763.292，時間戳記: 0xf3450dbf
例外狀況代碼: 0xc0000005
錯誤位移: 0x00045dbd
失敗的處理程序識別碼: 0xdfc
失敗的應用程式開始時間: 0x01d4dd34bec93d0d
失敗的應用程式路徑: C:\Program Files (x86)\Windows Media Player\wmplayer.exe
失敗的模組路徑: C:\WINDOWS\SYSTEM32\ntdll.dll
報告識別碼: 74487bb2-a784-43e9-80f8-c301ce211e53
失敗的套件完整名稱: 
失敗的套件相關應用程式識別碼: 
*/
